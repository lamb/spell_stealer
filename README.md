# Spell Stealer
## Spring 2019 - Game Programming (CIS 410/510)
## Corey Comish, Jacob Lambert, Bowen Hou

Contents
-------

  * ./Unity - Unity Project, with assets, scenes, etc.
  * ./PoC   - Deliverables for PoC build (documents, game executables)
  * ./Alpha - Deliverables for Alpha build (documents, game executables)
  * ./Beta  - Deliverables for Beta build (documents, game executables)
  * ./Final - Deliverables for Final build (documents, game executables)
