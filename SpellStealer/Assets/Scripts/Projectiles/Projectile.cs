﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int damage;
    public Transform source;

    private void OnTriggerEnter(Collider other)
    {
        if (source.tag == "Player" && other.tag == "Enemy")
        {
            print("Enemy HIT!");
            EnemyHealth enemyHealth = other.GetComponent<EnemyHealth>();
            enemyHealth.TakeDamage(damage);
            Destroy(gameObject);

        }

        if (source.tag == "Enemy" && other.tag == "Player")
        {
            print("Player HIT!");
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(damage);
            Destroy(gameObject);
        }

    }
}