﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public Transform source;

    private void OnTriggerEnter(Collider other)
    {
        if (source.tag != other.tag)
        {
            Destroy(gameObject);
        }
       
    }
}
