﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Collections;

enum SpellName
{
    lightRay,
    lightProj,
    lightAoe,
    lightSelf,
    plantProj,
    plantAoe,
    plantSelf,
    desertSelf,
    desertProj,
    desertAoe
}

enum SpellType
{
    proj,
    ray,
    aoe,
    self,
    empty
}

enum SpellEnv
{
    light,
    plant,
    desert
}

// Spell superclass
[System.Serializable]
public class Spell
{
    public int spellName;
    public int spellType;
    public int spellEnv;
    public Sprite spellImage;

    public int range;
    public float cooldown;
}

// Empty Spell
public class EmptySpell : Spell
{
    public EmptySpell()
    {
        spellImage = Resources.Load<Sprite>("Icons/empty");
        spellType = (int)SpellType.empty;
    }
}

// Ray Spell superclass
[System.Serializable]
public class RaySpell : Spell
{

    public int damage;

    public RaycastHit shootHit;
    public Ray shootRay;
    public Vector3 pos;

    public virtual void ShootRay(Transform source) { }
}

// Projectile Spell superclass
[System.Serializable]
public class ProjSpell : Spell
{
    public int damage;

    public Vector3  spellSpawn;
    public GameObject projObject;
}

// Self Spell superclass
[System.Serializable]
public class SelfSpell : Spell
{
    public int duration;
    public GameObject effectObj;

    public virtual void CastSelf() { }
}

// AOE Spell superclass
[System.Serializable]
public class AoeSpell : Spell
{
    public int damage;

    public GameObject aoeObject;
}

// Light Spells
[System.Serializable]
public class LightProj : ProjSpell
{
    public LightProj (GameObject source)
    {
        cooldown = 2.0f;
        spellName = (int)SpellName.lightProj;
        spellType = (int)SpellType.proj;
        spellEnv = (int)SpellEnv.light;

        spellImage = Resources.Load<Sprite>("Icons/lightProj");
        projObject = Resources.Load<GameObject>("Prefabs/Spells/Proj/lightProj");

        spellSpawn = new Vector3(0, 2f, 1f);
    }
}

[System.Serializable]
public class LightSelf : SelfSpell
{
    public GameObject source;

    public LightSelf (GameObject source)
    {
        cooldown = 5.0f;
        spellName = (int)SpellName.lightSelf;
        spellType = (int)SpellType.self;
        spellEnv = (int)SpellEnv.light;

        spellImage = Resources.Load<Sprite>("Icons/lightSelf");
        effectObj = Resources.Load<GameObject>("Prefabs/Spells/Self/lightSelf");

        this.source = source;
    }

    public override void CastSelf()
    {
        if (source.tag == "Player")
        {
            PlayerMovement playerMovement = source.GetComponent<PlayerMovement>();
            playerMovement.speed *= 1.75f;
            playerMovement.speedMod = true;
            playerMovement.speedModTimer = 2.0f;
        }

        if (source.tag == "Enemy")
        {
            EnemyMovement enemyMovement = source.GetComponent<EnemyMovement>();
            NavMeshAgent enemyNav = source.GetComponent<NavMeshAgent>();
            enemyNav.speed *= 1.75f;
            enemyMovement.speedMod = true;
            enemyMovement.speedModTimer = 2.0f;
        }

    }
}

[System.Serializable]
public class LightAoe : AoeSpell
{
    public LightAoe (GameObject source)
    {
        cooldown = 4.0f;
        spellName = (int)SpellName.lightAoe;
        spellType = (int)SpellType.aoe;
        spellEnv = (int)SpellEnv.light;

        spellImage = Resources.Load<Sprite>("Icons/lightAoe");
        aoeObject = Resources.Load<GameObject>("Prefabs/Spells/Aoe/lightAoeObject");
    }

}

[System.Serializable]
public class LightRay : RaySpell
{
    public LightRay (GameObject source)
    {
        cooldown = 1.0f;
        range = 10;
        damage = 20;
        spellName = (int)SpellName.lightRay;
        spellType = (int)SpellType.ray;
        spellEnv = (int)SpellEnv.light;

        spellImage = Resources.Load<Sprite>("Icons/lightRay");
        shootRay = new Ray();
    }

    public override void ShootRay(Transform source)
    {
        int shootableMask = LayerMask.GetMask("Shootable");

        pos = source.position;
        pos.y += 1;

        shootRay.origin = pos;
        shootRay.direction = source.forward;

        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
        {
            if (source.tag == "Player")
            {
                EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();

                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(damage);
                }
            }

            if (source.tag == "Enemy")
            {
                PlayerHealth playerHealth = shootHit.collider.GetComponent<PlayerHealth>();

                if (playerHealth != null)
                {
                    playerHealth.TakeDamage(damage);
                }
            }

        }

        LightningBoltScript lBS = source.GetComponent<LightningBoltScript>();
        lBS.Duration = 0.1f;
        lBS.StartPosition = pos;
        lBS.EndPosition = pos + source.forward * range;
        Debug.Log("Start: " + lBS.StartPosition.ToString());
        Debug.Log("End: " + lBS.EndPosition.ToString());
        lBS.Trigger();
    }
}

// Plant Spells
[System.Serializable]
public class PlantProj : ProjSpell
{
    public PlantProj(GameObject source)
    {
        cooldown = 1.0f;
        spellName = (int)SpellName.plantProj;
        spellType = (int)SpellType.proj;
        spellEnv = (int)SpellEnv.plant;

        spellImage = Resources.Load<Sprite>("Icons/plantProj");
        projObject = Resources.Load<GameObject>("Prefabs/Spells/Proj/plantProj");

        spellSpawn = new Vector3(0, 2f, 1f);
    }
}

[System.Serializable]
public class PlantAoe : AoeSpell
{
    public PlantAoe(GameObject source)
    {
        cooldown = 4.0f;
        spellName = (int)SpellName.plantAoe;
        spellType = (int)SpellType.aoe;
        spellEnv = (int)SpellEnv.plant;

        spellImage = Resources.Load<Sprite>("Icons/plantAoe");
        aoeObject = Resources.Load<GameObject>("Prefabs/Spells/Aoe/plantAoeObject");
    }

}

[System.Serializable]
public class PlantSelf : SelfSpell
{
    public GameObject source;

    public PlantSelf(GameObject source)
    {
        cooldown = 5.0f;
        spellName = (int)SpellName.plantSelf;
        spellType = (int)SpellType.self;
        spellEnv = (int)SpellEnv.plant;

        spellImage = Resources.Load<Sprite>("Icons/plantSelf");

        this.source = source;

        effectObj = Resources.Load<GameObject>("Prefabs/Spells/Self/plantSelf");
    }

    public override void CastSelf()
    {
        if (source.tag == "Player")
        {
            PlayerHealth playerHealth = source.GetComponent<PlayerHealth>();
            playerHealth.currentHealth += 20;
        }

        if (source.tag == "Enemy")
        {
            EnemyHealth enemyHealth = source.GetComponent<EnemyHealth>();
            enemyHealth.currentHealth += 20;
        }

    }
}

// Desert Spells
[System.Serializable]
public class DesertSelf : SelfSpell
{
    public GameObject source;

    public DesertSelf(GameObject source)
    {
        cooldown = 5.0f;
        spellName = (int)SpellName.desertSelf;
        spellType = (int)SpellType.self;
        spellEnv = (int)SpellEnv.desert;

        spellImage = Resources.Load<Sprite>("Icons/desertSelf");
        effectObj = Resources.Load<GameObject>("Prefabs/Spells/Self/desertSelf");

        this.source = source;
    }

    public override void CastSelf()
    {
        if (source.tag == "Player")
        {
            PlayerMovement playerMovement = source.GetComponent<PlayerMovement>();
            playerMovement.speed *= 20f;
            playerMovement.speedMod = true;
            playerMovement.speedModTimer = 0.1f;
        }

        if (source.tag == "Enemy")
        {
            EnemyMovement enemyMovement = source.GetComponent<EnemyMovement>();
            NavMeshAgent enemyNav = source.GetComponent<NavMeshAgent>();
            enemyMovement.nav.speed *= 20f;
            enemyMovement.speedMod = true;
            enemyMovement.speedModTimer = 0.1f;
        }

    }
}

[System.Serializable]
public class DesertProj : ProjSpell
{
    public DesertProj(GameObject source)
    {
        cooldown = 3.0f;
        spellName = (int)SpellName.desertProj;
        spellType = (int)SpellType.proj;
        spellEnv = (int)SpellEnv.desert;

        spellImage = Resources.Load<Sprite>("Icons/desertProj");
        projObject = Resources.Load<GameObject>("Prefabs/Spells/Proj/desertProj");

        spellSpawn = new Vector3(0, 2f, 1f);
    }
}

[System.Serializable]
public class DesertAoe : AoeSpell
{
    public DesertAoe(GameObject source)
    {
        cooldown = 6.0f;
        spellName = (int)SpellName.desertAoe;
        spellType = (int)SpellType.aoe;
        spellEnv = (int)SpellEnv.plant;

        spellImage = Resources.Load<Sprite>("Icons/desertAoe");
        aoeObject = Resources.Load<GameObject>("Prefabs/Spells/Aoe/desertAoeObject");
    }

}

public class PlayerAttack : MonoBehaviour
{
    Animator anim;
    float lTimer;
    float rTimer;
   

    public Spell lSpell;
    public Spell rSpell;
    public Spell eSpell;

    GameObject lSpellUI;
    GameObject rSpellUI;
    GameObject q0SpellUI, q1SpellUI, q2SpellUI, q3SpellUI, q4SpellUI;

    public Queue <Spell> spellQueue;
    GameObject[] spellUI;
     
    void Awake()
    { 
    anim = GetComponent<Animator>();

        // Default spells
        //lSpell = new PlantSelf(gameObject);
        lSpell = new DesertSelf(gameObject);
        rSpell = new DesertProj(gameObject);
        lSpellUI = GameObject.Find("LeftSpell");
        lSpellUI.GetComponent<Image>().sprite = lSpell.spellImage;
        rSpellUI = GameObject.Find("RightSpell");
        rSpellUI.GetComponent<Image>().sprite = rSpell.spellImage;

        // Spell queue
        spellUI = new GameObject[5];
        spellUI[0] = GameObject.Find("QueueSpell0");
        spellUI[1] = GameObject.Find("QueueSpell1");
        spellUI[2] = GameObject.Find("QueueSpell2"); 
        spellUI[3] = GameObject.Find("QueueSpell3");
        spellUI[4] = GameObject.Find("QueueSpell4");

        spellQueue = new Queue<Spell>();
        spellQueue.Enqueue(new LightProj(gameObject));
        spellQueue.Enqueue(new LightRay(gameObject));
        spellQueue.Enqueue(new LightSelf(gameObject));
        spellQueue.Enqueue(new LightAoe(gameObject));
        spellQueue.Enqueue(new PlantProj(gameObject));


        Spell[] spellArray = spellQueue.ToArray();
        for (int i = 0; i < 5; ++i)
        {
            spellUI[i].GetComponent<Image>().sprite = spellArray[i].spellImage;
        }
    }

    void Update()
    {
        lTimer += Time.deltaTime;
        rTimer += Time.deltaTime;

        bool isShiftKeyDown = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

        // Shift R
        if (isShiftKeyDown && Input.GetKeyDown("r")) {

            Spell tmp = lSpell;
            lSpell = rSpell;
            rSpell = tmp;

            lSpellUI.GetComponent<Image>().sprite = lSpell.spellImage;
            rSpellUI.GetComponent<Image>().sprite = rSpell.spellImage;
        }
        // Shift left click
        if (isShiftKeyDown && Input.GetButtonDown("Fire1"))
        {
            // Don't load empty spells
            if (spellQueue.Peek().spellType == (int)SpellType.empty)
            {
                spellQueue.Dequeue();
                spellQueue.Enqueue(new EmptySpell());
                print("Don't load empty spell");
                UpdateSpellQueueIcons();
                return;
            }

            lSpell = spellQueue.Dequeue();
            lSpellUI.GetComponent<Image>().sprite = lSpell.spellImage;

            spellQueue.Enqueue(new EmptySpell());
            UpdateSpellQueueIcons();
        }
        // Shift right click
        else if (isShiftKeyDown && Input.GetButtonDown("Fire2"))
        {
            // Don't load empty spells
            if (spellQueue.Peek().spellType == (int)SpellType.empty)
            {
                spellQueue.Dequeue();
                spellQueue.Enqueue(new EmptySpell());
                print("Don't load empty spell");
                UpdateSpellQueueIcons();
                return;
            }

            rSpell = spellQueue.Dequeue();
            rSpellUI.GetComponent<Image>().sprite = rSpell.spellImage;

            spellQueue.Enqueue(new EmptySpell());
            UpdateSpellQueueIcons();
        }
        // Left click
        else if (Input.GetButtonDown("Fire1") && lTimer >= lSpell.cooldown && Time.timeScale != 0f)
        {
            anim.SetTrigger("AttackL");
            lTimer = 0f;

            if (lSpell.spellType == (int)SpellType.proj)
            {
                GameObject spellObject = Instantiate(((ProjSpell)lSpell).projObject,
                            ((ProjSpell)lSpell).spellSpawn + transform.position,
                            transform.rotation);
                SetSourceTag(spellObject, transform);


            }

            if (lSpell.spellType == (int)SpellType.ray)
            {
                ((RaySpell) lSpell).ShootRay(transform);
            }

            if (lSpell.spellType == (int)SpellType.aoe)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray))
                {
                    Vector3 mouse = Input.mousePosition;
                    Ray castPoint = Camera.main.ScreenPointToRay(mouse);
                    RaycastHit hit;
                    if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
                    {

                        GameObject spellObject = Instantiate(((AoeSpell)lSpell).aoeObject,
                                    hit.point,
                                    transform.rotation);
                        SetSourceTag(spellObject, transform);
                    }
                }
            }

            if (lSpell.spellType == (int)SpellType.self)
            {
                ((SelfSpell)lSpell).CastSelf();

                GameObject spellObject = Instantiate(((SelfSpell)lSpell).effectObj,
                           transform.position,
                            transform.rotation);
                SetSourceTag(spellObject, transform);
            }

        }
        // Right click
        else if (Input.GetButtonDown("Fire2") && rTimer >= rSpell.cooldown && Time.timeScale != 0f)
        {
            anim.SetTrigger("AttackR");
            rTimer = 0f;


            if (rSpell.spellType == (int)SpellType.proj)
            {
                GameObject spellObject = Instantiate(((ProjSpell)rSpell).projObject,
                            ((ProjSpell)rSpell).spellSpawn + transform.position,
                            transform.rotation);
                SetSourceTag(spellObject, transform);


            }

            if (rSpell.spellType == (int)SpellType.ray)
            {
                ((RaySpell)rSpell).ShootRay(transform);
        
            }

            if (rSpell.spellType == (int)SpellType.aoe)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray))
                {
                    Vector3 mouse = Input.mousePosition;
                    Ray castPoint = Camera.main.ScreenPointToRay(mouse);
                    RaycastHit hit;
                    if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
                    {

                        GameObject spellObject = Instantiate(((AoeSpell)rSpell).aoeObject,
                                    hit.point,
                                    transform.rotation);
                        SetSourceTag(spellObject, transform);
                    }
                }
            }
            if (rSpell.spellType == (int)SpellType.self)
            {
                ((SelfSpell)rSpell).CastSelf();

                GameObject spellObject = Instantiate(((SelfSpell)rSpell).effectObj,
                           transform.position,
                           transform.rotation);
                SetSourceTag(spellObject, transform);
            }

        }
    }

    public static void SetSourceTag(GameObject spellObject, Transform transform)
    {
        if (spellObject.GetComponent<Projectile>() != null)
            spellObject.GetComponent<Projectile>().source = transform;
        if (spellObject.GetComponent<Stun>() != null)
            spellObject.GetComponent<Stun>().source = transform;
        if (spellObject.GetComponent<DestroyByContact>() != null)
            spellObject.GetComponent<DestroyByContact>().source = transform;
        if (spellObject.GetComponent<Dot>() != null)
            spellObject.GetComponent<Dot>().source = transform;
        if (spellObject.GetComponent<EffectMover>() != null)
            spellObject.GetComponent<EffectMover>().source = transform;

    }

    public void UpdateSpellQueueIcons()
    {
        Spell[] spellArray = spellQueue.ToArray();
        for (int i = 0; i < 5; ++i)
        {
            spellUI[i].GetComponent<Image>().sprite = spellArray[i].spellImage;
        }
    }
}