﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 4f;
    public float baseSpeed;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;

    public bool speedMod;
    public float speedModTimer;

    public bool stunned;
    public float stunTimer;

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();

        baseSpeed = speed;
    }

    void FixedUpdate()
    {
        if (speedMod)
        {
            speedModTimer -= Time.deltaTime;
            if (speedModTimer < 0)
            {
                speedMod = false;
                speed = baseSpeed;
            }
        }

        if (stunned)
        {
            stunTimer -= Time.deltaTime;
            if (stunTimer < 0)
            {
                stunned = false;
            }
            else
            {
                return;
            }
        }

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        // if moving, trigger locomotion animation
        if (h != 0.0f || v != 0.0f)
        {
            anim.SetFloat("Speed", 1.0f);
        }
        else { anim.SetFloat("Speed", 0.0f); }


        Move(h, v);
        Turning();
    }

    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;
        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
        }
    }
}
