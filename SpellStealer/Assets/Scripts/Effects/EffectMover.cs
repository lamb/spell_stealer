﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectMover : MonoBehaviour
{
    public Transform source;

    void Update()
    {
        GetComponent<Rigidbody>().position = source.position +
                                            new Vector3(0.0f, 2.0f, 0.0f);
    }
}
