﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour
{
    float timer;
    public int damage;
    private List<Collider> colliders;
    public Transform source;

    // Start is called before the first frame update
    void Start()
    {
        colliders = new List<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        // Damage enemies in cloud twice per second
        if (timer > 0.5f)
        {
            foreach (Collider col in colliders.ToArray())
            {
                if (col == null) colliders.Remove(col);

                if (col.tag == "Enemy" && source.tag == "Player")
                {
                    print("ENEMY TAKING DAMAGE FROM POISON");
                    EnemyHealth enemyHealth = col.GetComponent<EnemyHealth>();

                    if (enemyHealth.currentHealth < damage)
                    {
                        colliders.Remove(col);
                    }

                    enemyHealth.TakeDamage(damage);
                }

                if (col.tag == "Player" && source.tag == "Enemy")
                {
                    print("PLAYER TAKING DAMAGE FROM POISON");
                    PlayerHealth playerHealth = col.GetComponent<PlayerHealth>();

                    if (playerHealth.currentHealth < damage)
                    {
                        colliders.Remove(col);
                    }

                    playerHealth.TakeDamage(damage);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!colliders.Contains(other)) {
        colliders.Add(other); }
    }

    private void OnTriggerExit(Collider other)
    {
        colliders.Remove(other);
    }
}