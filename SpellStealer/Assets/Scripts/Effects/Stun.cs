﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stun : MonoBehaviour
{
    public float stunTimer;
    public Transform source;

    private void OnTriggerEnter(Collider other)
    {

        if (source.tag == "Player" && other.tag == "Enemy")
        {
            EnemyMovement enemyMovement = other.GetComponent<EnemyMovement>();
            print("Enemy Stunned!");
            Debug.Log(other.material.name);
            enemyMovement.stunned = true;
            enemyMovement.stunTimer = stunTimer;
        }

        if (source.tag == "Enemy" && other.tag == "Player")
        {
            PlayerMovement playerMovement = other.GetComponent<PlayerMovement>();
            print("Player Stunned!");
            Debug.Log(other.material.name);
            playerMovement.stunned = true;
            playerMovement.stunTimer = stunTimer;
        }
    }
}
