﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemiesRemaining : MonoBehaviour
{
    private int eRem;
    Text text;
    public string sceneName;
    GameObject[] enemies;
    private static int cCount = 0;

    void Awake()
    {
        text = GetComponent<Text>();
        sceneName = SceneManager.GetActiveScene().name;
    }

    void Update()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        eRem = enemies.Length;
        text.text = "Enemies Remaining: " + eRem;
        //if (eRem == 0 && sceneName != "Level00_Hub")
        //{
        //    cCount++;
        //    if (cCount > 3)
        //    {
        //        SceneManager.LoadScene("Level00_HubBoss");
        //    }
        //    SceneManager.LoadScene("Level00_Hub");
        //}
    }
}
