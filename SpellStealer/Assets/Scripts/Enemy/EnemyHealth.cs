﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 0.5f;
    public string charName;
    public Slider healthSlider;
    public Text enemyName;

    Animator anim;
    CapsuleCollider capsuleCollider;
    bool isDead;

    void Awake ()
    {
        anim = GetComponent <Animator> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();
        currentHealth = startingHealth;
    }

    //public void TakeDamage(int amount, string action);
    public void TakeDamage(int amount)
    {

        healthSlider.gameObject.SetActive(true);
        enemyName.gameObject.SetActive(true);
        enemyName.text = charName;
        string action = "left";
        if (isDead)
            return;
        currentHealth -= amount;
        healthSlider.value = currentHealth;
        if (currentHealth <= 0)
        {
            Death(action);
        }
        else { anim.SetTrigger("Hit"); }
    }

    void Death (string action)
    {
        isDead = true;
        healthSlider.gameObject.SetActive(false);
        enemyName.gameObject.SetActive(false);
        capsuleCollider.isTrigger = true;
        anim.SetTrigger ("Dead");
        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;

        // Add spells to Queue
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        EnemyAttack enemyAttack = GetComponent<EnemyAttack>();
        PlayerAttack playerAttack = player.GetComponent<PlayerAttack>();

        foreach (Spell sp in enemyAttack.spellList)
        {
            playerAttack.spellQueue.Dequeue();
            //playerAttack.spellQueue.Enqueue(sp);


            if (sp.spellName == (int)SpellName.lightRay) playerAttack.spellQueue.Enqueue(new LightRay(player));
            if (sp.spellName == (int)SpellName.lightProj) playerAttack.spellQueue.Enqueue(new LightProj(player));
            if (sp.spellName == (int)SpellName.lightAoe) playerAttack.spellQueue.Enqueue(new LightAoe(player));
            if (sp.spellName == (int)SpellName.lightSelf) playerAttack.spellQueue.Enqueue(new LightSelf(player));
            if (sp.spellName == (int)SpellName.plantProj) playerAttack.spellQueue.Enqueue(new PlantProj(player));
            if (sp.spellName == (int)SpellName.plantAoe) playerAttack.spellQueue.Enqueue(new PlantAoe(player));
            if (sp.spellName == (int)SpellName.plantSelf) playerAttack.spellQueue.Enqueue(new PlantSelf(player));
            if (sp.spellName == (int)SpellName.desertSelf) playerAttack.spellQueue.Enqueue(new DesertSelf(player));
            if (sp.spellName == (int)SpellName.desertProj) playerAttack.spellQueue.Enqueue(new DesertProj(player));
            if (sp.spellName == (int)SpellName.desertAoe) playerAttack.spellQueue.Enqueue(new DesertAoe(player));
                      

        }

        playerAttack.UpdateSpellQueueIcons();

        Destroy(gameObject, 3f);
    }
}
