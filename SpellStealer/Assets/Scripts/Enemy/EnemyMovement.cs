﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    Animator anim;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    public NavMeshAgent nav;
    public Vector3 enemyDirection;
    public int aggroRange;

    public bool stunned;
    public float stunTimer;

    public bool speedMod;
    public float speedModTimer;
    public float baseSpeed;

    float timer;

    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent <PlayerHealth>();
        enemyHealth = GetComponent <EnemyHealth>();
        nav = GetComponent <NavMeshAgent>();
        anim = GetComponent<Animator>();
        anim.SetBool("Moving", false);
        stunned = false;

        baseSpeed = nav.speed;
    }

    void Update ()
    {
        if (stunned)
        {
            stunTimer -= Time.deltaTime;
            if (stunTimer < 0)
            {
                stunned = false;
                nav.enabled = true;
            }
        }

        if (speedMod)
        {
            print("ENEMY SPEED MOD");
            speedModTimer -= Time.deltaTime;
            if (speedModTimer < 0)
            {
                speedMod = false;
                nav.speed = baseSpeed;
            }
        }

        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0 
            && !stunned)
        {
            float distToPlayer = Vector3.Distance(transform.position, 
                                     player.transform.position);

            if (distToPlayer <= aggroRange ||
                enemyHealth.currentHealth < enemyHealth.startingHealth)
            {
                anim.SetBool("Moving", true);
                nav.SetDestination(player.position);
                enemyDirection = transform.forward;
            }
        }
        else
        {
            anim.SetBool("Moving", false);
            nav.enabled = false;
        }
    }
}
