﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    public string attackAnim;

    public int cast_range;
    bool playerInCastRange;

    public bool melee;
    public int melee_range;
    public int melee_damage;
    public float melee_cd;
    float meleeTimer;
    bool playerInMeleeRange;

    Animator anim;
    GameObject player;
    EnemyHealth enemyHealth;

    public bool lightRay;
    public bool lightProj;
    public bool lightAoe;
    public bool lightSelf;
    public bool plantProj;
    public bool plantAoe;
    public bool plantSelf;
    public bool desertSelf;
    public bool desertProj;
    public bool desertAoe;

    public List<Spell> spellList;
    List<float> cdList;
    List<float> timerList;

    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent <Animator> ();

        spellList = new List<Spell>();
        if (lightRay)   spellList.Add(new LightRay(transform.gameObject));
        if (lightProj)  spellList.Add(new LightProj(transform.gameObject));
        if (lightAoe)   spellList.Add(new LightAoe(transform.gameObject));
        if (lightSelf)  spellList.Add(new LightSelf(transform.gameObject));
        if (plantProj)  spellList.Add(new PlantProj(transform.gameObject));
        if (plantAoe)   spellList.Add(new PlantAoe(transform.gameObject));
        if (plantSelf)  spellList.Add(new PlantSelf(transform.gameObject));
        if (desertSelf) spellList.Add(new DesertSelf(transform.gameObject));
        if (desertProj) spellList.Add(new DesertProj(transform.gameObject));
        if (desertAoe)  spellList.Add(new DesertAoe(transform.gameObject));

        cdList = new List<float>();
        timerList = new List<float>();
        foreach (Spell sp in spellList)
        {
            cdList.Add(sp.cooldown);
            timerList.Add(0);
        }

    }

    void Update ()
    {
        playerInCastRange = Vector3.Distance(transform.position, player.transform.position) < cast_range;
        playerInMeleeRange = Vector3.Distance(transform.position, player.transform.position) < melee_range;

        // Melee
        meleeTimer += Time.deltaTime;
        if (melee && playerInMeleeRange && enemyHealth.currentHealth > 0 && meleeTimer > melee_cd)
        {
            anim.SetTrigger(attackAnim);
            player.GetComponent<PlayerHealth>().TakeDamage(melee_damage);
            meleeTimer = 0f;
        }

        // Spells
        if (playerInCastRange)
        {

            for (int i = 0; i < spellList.Count; ++i)
            {
                timerList[i] += Time.deltaTime;

                if (timerList[i] > cdList[i])
                {
                    CastSpell(spellList[i]);
                    timerList[i] = 0f;
                }

            }
        }

    }

    void CastSpell(Spell sp)
    {
        if (sp.spellType == (int)SpellType.proj)
        {
            GameObject spellObject = Instantiate(((ProjSpell)sp).projObject,
                        ((ProjSpell)sp).spellSpawn + transform.position,
                        transform.rotation);
            PlayerAttack.SetSourceTag(spellObject, transform);
        }

        if (sp.spellType == (int)SpellType.self)
        {
            ((SelfSpell)sp).CastSelf();

            GameObject spellObject = Instantiate(((SelfSpell)sp).effectObj,
                       transform.position,
                       transform.rotation);
            PlayerAttack.SetSourceTag(spellObject, transform);
        }

        if (sp.spellType == (int)SpellType.aoe)
        {
            Vector3 target = transform.position + transform.forward * 10;
            // Not implemented yet
            GameObject spellObject = Instantiate(((AoeSpell)sp).aoeObject,
                target,
                transform.rotation);
            PlayerAttack.SetSourceTag(spellObject, transform);
        }

        if (sp.spellType == (int)SpellType.ray)
        {
            ((RaySpell)sp).ShootRay(transform);
        }

    }
       

    }

